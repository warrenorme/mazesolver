import cv2 as cv
import numpy as numpy
import matplotlib.pyplot as plt
#HANDLES IMAGE PROCESSING
imgPath = "test2.png"
img = cv.imread(imgPath,0) #GETS PIXEL VALUES FROM IMAGE INTO AN ARRAY
imgReverted = cv.bitwise_not(img) # FLIPS THE VALUE
finalImage = imgReverted // 255 # TURNS IN TO BLACK AND WHITE VALUE
path = numpy.zeros((len(finalImage),(len(finalImage[0]))),dtype=int) # Creates a clone of the array with all zeros used to find visted nodes
start = [0]
end = [len(finalImage)-1]
#HANDELS GETTING THE START AND FINISH LOCATION OF THE MAZE
for i in range(len(finalImage[0])):
    if finalImage[0][i] == 0:
        start.append(i)
    if finalImage[len(finalImage)-1][i] == 0:
        end.append(i)
print(end[1])
path[start[0]][start[1]] = 1
#HANDELS CREATING THE PATH VALUES 
def step(currentvalue,path): #EACH STEP FOR THE FLOOD FILL SEARCH
    for i in range(len(path)):
        for x in range(len(path[i])):
            if path[i][x] == currentvalue:
                print(finalImage[i-1][x],finalImage[i][x-1],finalImage[i+1][x],finalImage[i+1][x])
                print(path[i-1][x],path[i][x-1], path[i+1][x],path[i][x])
                if i>0 and path[i-1][x] == 0 and finalImage[i-1][x] == 0:
                    path[i-1][x] = currentvalue + 1
                if x>0 and path[i][x-1] == 0 and finalImage[i][x-1] == 0:
                    path[i][x-1] = currentvalue + 1
                if i<len(path)-1 and path[i+1][x] == 0 and finalImage[i+1][x] == 0:
                    path[i+1][x] = currentvalue + 1
                if x<len(path[i])-1 and path[i][x+1] == 0 and finalImage[i][x+1] == 0:
                    path[i][x+1] = currentvalue + 1
currentvalue = 0
while path[end[0]][end[1]] == 0:
    currentvalue = currentvalue + 1
    step(currentvalue,path)
print("finished")
finalPath = [(end[0],end[1])]
i = end[0]
j = end[1]
currentvalue = currentvalue + 1
while currentvalue > 1:
    if i > 0 and path[i-1][j] == currentvalue - 1:
        i,j = i-1,j
        finalPath.append((i,j))
        currentvalue = currentvalue - 1
    elif j>0 and path[i][j-1] == currentvalue -1:
        i,j = i,j-1
        finalPath.append((i,j))
        currentvalue = currentvalue - 1
    elif  i <len(path) - 1 and path[i+1][j] == currentvalue -1:
        i,j = i+1,j 
        finalPath.append((i,j))
        currentvalue = currentvalue - 1
    elif j <len(path[i]) -1 and path[i][j+1] == currentvalue -1:
        i,j = i,j+1
        finalPath.append((i,j))
        currentvalue = currentvalue - 1
print(finalPath)
newImage = img
print(newImage)
print(newImage[1][1])
for i in range(len(finalPath)):
    newImage[finalPath[i][0]][finalPath[i][1]] = 200
plt.imshow(newImage)
plt.pause(0)

